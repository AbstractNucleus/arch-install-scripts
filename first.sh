. ./credentials;

sudo pacman -Sy archlinux-keyring reflector --noconfirm;
echo "Installed arch keyring and reflector";

reflector --country 'Sweden' --protocol https --sort rate --save /etc/pacman.d/mirrorlist;
mkdir -p /mnt/etc/pacman.d;
touch /mnt/etc/pacman.d/mirrorlist;
reflector --country 'Sweden' --protocol https --sort rate --save /mnt/etc/pacman.d/mirrorlist;
echo "Filtered swedish mirrors and sorted them by speed";

pacstrap /mnt base base-devel linux linux-firmware vim sysfsutils usbutils e2fsprogs dosfstools mtools inetutils netctl device-mapper cryptsetup nano less lvm2 dialog wpa_supplicant reflector xdg-user-dirs xdg-utils --noconfirm;
pacstrap /mnt linux-headers go arch-install-scripts bind-tools amd-ucode intel-ucode dkms p7zip archiso mkinitcpio-archiso haveged pacman-contrib pkgfile git diffutils jfsutils reiserfsprogs btrfs-progs f2fs-tools logrotate man-db man-pages mdadm perl s-nail texinfo which xfsprogs lsscsi sdparm sg3_utils smartmontools fuse2 fuse3 ntfs-3g exfat-utils gvfs gvfs-afc gvfs-goa gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb unrar unzip unace xz xdg-user-dirs grsync ddrescue dd_rescue testdisk hdparm htop rsync hardinfo bash-completion geany lsb-release polkit ufw bleachbit packagekit gparted qt5ct lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings accountsservice meld nvme-cli devtools pulseaudio vlc simplescreenrecorder cdrtools gstreamer gst-libav gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly gstreamer-vaapi xvidcore frei0r-plugins cdrdao dvdauthor transcode alsa-utils alsa-plugins alsa-firmware pulseaudio-alsa pulseaudio-equalizer pulseaudio-jack ffmpeg ffmpegthumbnailer libdvdcss gimp guvcview imagemagick flac faad2 faac mjpegtools x265 x264 lame sox mencoder b43-fwcutter broadcom-wl-dkms ipw2100-fw ipw2200-fw gnu-netcat net-tools networkmanager networkmanager-openvpn nm-connection-editor network-manager-applet wget curl firefox thunderbird wireless_tools nfs-utils nilfs-utils dhclient dnsmasq dmraid dnsutils openvpn openssh openssl samba whois iwd filezilla avahi openresolv youtube-dl vsftpd wpa_supplicant system-config-printer foomatic-db foomatic-db-engine gutenprint hplip simple-scan cups cups-pdf cups-filters cups-pk-helper ghostscript gsfonts python-pillow python-pyqt5 python-pip python-reportlab ttf-ubuntu-font-family ttf-dejavu ttf-bitstream-vera ttf-liberation noto-fonts ttf-roboto ttf-opensans opendesktop-fonts cantarell-fonts freetype2 papirus-icon-theme ttf-font-awesome ttf-droid ttf-inconsolata terminus-font xorg xorg-apps xorg-server xorg-drivers xorg-xkill xorg-xinit xterm mesa git base-devel --noconfirm;
arch-chroot /mnt systemctl enable haveged.service;
arch-chroot /mnt systemctl enable org.cups.cupsd.service;
arch-chroot /mnt systemctl enable NetworkManager.service;
echo "Installed packages and enabled services to the new system";

arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime;
arch-chroot /mnt hwclock --systohc;
echo "Timezone set to Europe/Stockholm";

genfstab -U /mnt >> /mnt/etc/fstab;
echo "Generated fstab file in the new system";

echo en_US.UTF-8 UTF-8 > /mnt/etc/locale.gen;
arch-chroot /mnt locale-gen;
echo LANG=en_US.UTF-8 > /mnt/etc/locale.conf;
echo KEYMAP=sv-latin1 > /mnt/etc/vconsole.conf;
echo "Locale and keymap has been set";

echo ${HSTNAME} > /mnt/etc/hostname;
echo "Hostname has been set to ${HSTNAME}";
arch-chroot /mnt useradd -m -G sys,log,network,floppy,scanner,power,rfkill,users,video,storage,optical,lp,audio,wheel,adm -s /bin/bash ${USRNAME};
echo "User ${USRNAME} has been added";
arch-chroot /mnt chpasswd <<< ""${USRNAME}":"${USRPASSWD}"";
arch-chroot /mnt chpasswd <<< "root:"${RTPASSWD}"";

echo "127.0.0.1   localhost" > /mnt/etc/hosts;
echo "::1         localhost" >> /mnt/etc/hosts;
echo "127.0.1.1   ${HSTNAME}.localdomain ${HSTNAME}" >> /mnt/etc/hosts;
echo "Hosts has been set"

arch-chroot /mnt pacman -S grub efibootmgr os-prober --noconfirm;
arch-chroot /mnt grub-install --target=x86_64-efi --efi-directory=/efi --bootloader-id=GRUB --recheck;
arch-chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg;
echo "Grub has been installed";

cp second.sh /mnt/home/${USRNAME}/;
cp credentials /mnt/home/${USRNAME}/;
cp -r Window\ Managers/ /mnt/home/${USRNAME}/;
cp -r Desktop\ Environments/ /mnt/home/${USRNAME}/;

awk 'NR==82 {$0="%wheel ALL=(ALL) ALL"} 1' /mnt/etc/sudoers;
awk 'NR==85 {$0="%wheel ALL=(ALL) NOPASSWD: ALL"} 1' /mnt/etc/sudoers;
awk 'NR==88 {$0="%sudo ALL=(ALL) ALL"} 1' /mnt/etc/sudoers;
echo "${USRNAME} ALL=(ALL) NOPASSWD: ALL" >> /mnt/etc/sudoers;

echo "_________________________________";
echo "";
echo "Now 'umount -a' and 'reboot'";
echo "";
echo "After booting up the computer again, login as your user and if needed connect to wifi with 'wifi-menu' and then run second.sh";
echo "";
echo "_________________________________"
