# Pre-requisites

#### Set keyboard layout:
```loadkeys sv-latin1```

#### Set time:
```timedatectl set-ntp true```


#### Connect to wifi:
```
iwctl
station wlan0 scan
station wlan0 get-networks
station wlan0 connect ESSID
```

#### Partition disks:
Needed partitioning scheme for this install:
    (efi, root) = ex:(sda1, sda2)

Partition your disk:
    ```cfdisk /dev/sda```

Format efi partition (skip this if dual-booting):
    ```mkfs.fat -F32 /dev/sda1```

Format root partition:
    ```mkfs.ext4 /dev/sda2```

Mount root:
    ```mount /dev/sda2 /mnt```

Create efi directory:
    ```mkdir /mnt/efi```

Mount efi:
    ```mount /dev/sda1 /mnt/efi```

# Installation

### Editing credentials
Before installing you need to edit the credentials file:
    ```vim credentials.sh```

### Installing
Run the first.sh script
    ```./first.sh```

After running the script your computer will reboot and you will get prompted to log in. 
Login with your username and your password that you chose when editing the credentials file.
If needed you can connect to wifi:
    ```wifi-menu```

Then run the second.sh script to install yay and a desktop environment of your choice.

# Post Install
### Here are some tips on what you can do after install.
	#### Switch to ZSH shell instead of bash:
	```
	hej
	```

	#### Install video drivers for you graphics card:
	Just use yay or pacman to install;	
	
	Intel
		```xf86-video-intel```
	
	Amd
		```xf86-video-amdgpu``` or ```xf86-video-ati```

	Nvidia
		```xf86-video-nouveau``` or ```nvidia``` and ```nvidia-utils``` and ```nvidia-settings```
