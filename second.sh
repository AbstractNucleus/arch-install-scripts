. ./credentials;

mkdir /home/${USRNAME}/Downloads;
cd /home/${USRNAME}/Downloads;
git clone https://aur.archlinux.org/yay.git;
cd /home/${USRNAME}/Downloads/yay;
makepkg -si;
echo "Installed yay.";

echo ""
echo "Now you can choose which desktop environment or window manager you want: ";
echo "1) i3";
echo "2) gnome";
echo "3) lxqt";
echo "4) plasma";
echo "5) xfce";
echo "6) mate";
echo "7) cinnamon)";
echo "Choose one option: ";
read choice;
case $choice in
    1) ./../../Window\ Managers/i3.sh;;
    2) ./../../Desktop\ Environments/gnome.sh;;
    3) ./../../Desktop\ Environments/lxqt.sh;;
    4) ./../../Desktop\ Environments/plasma.sh;;
    5) ./../../Desktop\ Environments/xfce.sh;;
    6) ./../../Desktop\ Environments/mate.sh;;
    7) ./../../Desktop\ Environments/cinnamon.sh;;
    *) echo "invalid option";;
esac

sudo rm -r ./../../Desktop\ Environments/;
sudo rm -r ./../../Window\ Managers/;
sudo rm -r ./../../files/;
sudo rm -r ./../../credentials;
sudo rm -r ./../../second.sh
