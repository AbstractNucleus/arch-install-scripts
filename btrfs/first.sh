loadkeys sv-latin1;
timedatectl set-ntp true;
reflector -c Sweden --age 10 --sort rate --save /etc/pacman.d/mirrorlist

# Maybe change value nvme0n1p5
mkfs.btrfs /dev/nvme0n1p5;
mount /dev/nvme0n1p5 /mnt;
btrfs su cr /mnt/@root;
btrfs su cr /mnt/@home;
btrfs su cr /mnt/@var;
btrfs su cr /mnt/@srv;
btrfs su cr /mnt/@opt;
btrfs su cr /mnt/@tmp;
btrfs su cr /mnt/@swap;
btrfs su cr /mnt/@.snapshots;
umount /mnt;
mount -o noatime,compress=lzo,space_cache,subvol=@root /dev/nvme0n1p5 /mnt;
mkdir /mnt/{boot,home,var,opt,srv,tmp,swap,.snapshots};
mount -o noatime,compress=lzo,space_cache,subvol=@home /dev/nvme0n1p5 /mnt/home;
mount -o noatime,compress=lzo,space_cache,subvol=@tmp /dev/nvme0n1p5 /mnt/tmp;
mount -o noatime,compress=lzo,space_cache,subvol=@opt /dev/nvme0n1p5 /mnt/opt;
mount -o noatime,compress=lzo,space_cache,subvol=@srv /dev/nvme0n1p5 /mnt/srv;
mount -o noatime,compress=lzo,space_cache,subvol=@.snapshots /dev/nvme0n1p5 /mnt/.snapshots;
mount -o nodatacow,subvol=@swap /dev/nvme0n1p5 /mnt/swap;
mount -o nodatacow,subvol=@var /dev/nvme0n1p5 /mnt/var;
mount /dev/nvme0n1p1 /mnt/boot;

# Pacstrap
pacstrap /mnt base base-devel linux linux-firmware vim amd-ucode btrfs-progs sysfsutils usbutils e2fsprogs dosfstools mtools inetutils device-mapper dialog wpa_supplicant reflector xdg-utils less go arch-install-scripts bind-tools dkms p7zip haveged pacman-contrib pkgfile git diffutils jfsutils reiserfsprogs f2fs-tools logrotate man-db man-pages mdadm perl s-nail texinfo which xfsprogs lsscsi sdparm sg3_utils smartmontools fuse2 fuse3 ntfs-3g exfat-utils gvfs gvfs-afc gvfs-goa gvfs-gphoto2 gvfs-mtp gvfs-nfs gvfs-smb unrar unzip unace xz xdg-user-dirs grsync ddrescue dd_rescue testdisk hdparm htop rsync hardinfo bash-completion geany lsb-release polkit ufw bleachbit packagekit gparted qt5ct accountsservice meld nvme-cli devtools vlc simplescreenrecorder cdrtools gstreamer gst-libav gst-plugins-base gst-plugins-good gst-plugins-bad gst-plugins-ugly gstreamer-vaapi xvidcore frei0r-plugins cdrdao dvdauthor transcode ffmpeg ffmpegthumbnailer libdvdcss gimp guvcview imagemagick flac faad2 faac mjpegtools x265 x264 lame sox mencoder b43-fwcutter broadcom-wl-dkms ipw2100-fw ipw2200-fw gnu-netcat net-tools networkmanager networkmanager-openvpn nm-connection-editor network-manager-applet wget curl wireless_tools nfs-utils nilfs-utils dhclient dnsmasq dmraid dnsutils openvpn openssh openssl samba whois iwd avahi openresolv youtube-dl vsftpd system-config-printer foomatic-db foomatic-db-engine gutenprint hplip simple-scan cups cups-pdf cups-filters cups-pk-helper ghostscript gsfonts python-pillow python-pyqt5 python-pip python-reportlab ttf-ubuntu-font-family ttf-dejavu ttf-bitstream-vera ttf-liberation noto-fonts ttf-roboto ttf-opensans opendesktop-fonts cantarell-fonts freetype2 papirus-icon-theme ttf-font-awesome ttf-droid ttf-inconsolata terminus-font xorg xorg-apps xorg-server xorg-drivers xorg-xkill xorg-xinit xterm nvidia nvidia-utils nvidia-settings gdm gnome gnome-extra accerciser dconf-editor ghex gnome-sound-recorder gnome-todo gnome-tweaks gnome-usage sysprof gnome-nettool chrome-gnome-shell gnome-shell-extensions adapta-gtk-theme arc-gtk-theme arc-icon-theme gtk-engine-murrine gnome-keyring transmission-gtk brasero asunder quodlibet gnome-disk-utility polkit-gnome gnome-packagekit linux-headers grub grub-btrfs efibootmgr os-prober bluez bluez-utils --noconfirm;


genfstab -U /mnt >> /mnt/etc/fstab;
arch-chroot /mnt truncate -s 0 /swap/swapfile;
arch-chroot /mnt chattr +C /swap/swapfile;
arch-chroot /mnt btrfs property set /swap/swapfile compression none;
arch-chroot /mnt dd if=/dev/zero of=/swap/swapfile bs=1G count=8 status=progress;
arch-chroot /mnt chmod 600 /swap/swapfile;
arch-chroot /mnt mkswap /swap/swapfile;
arch-chroot /mnt swapon /swap/swapfile;
arch-chroot /mnt echo "" >> /etc/fstab;
arch-chroot /mnt echo "/swap/swapfile none swap defaults 0 0" >> /etc/fstab;
arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime;
arch-chroot /mnt hwclock --systohc;
arch-chroot /mnt echo "en_US.UTF-8 UTF-8" > /etc/locale.gen;
arch-chroot /mnt locale-gen;
arch-chroot /mnt echo "LANG=en_US.UTF-8" >> /etc/locale.conf;
arch-chroot /mnt echo "KEYMAP=sv-latin1" >> /etc/vconsole.conf;
arch-chroot /mnt echo "Nucleus" > /etc/hostname;
arch-chroot /mnt echo "127.0.0.1    localhost" >> /etc/hosts;
arch-chroot /mnt echo "::1          localhost" >> /etc/hosts;
arch-chroot /mnt echo "127.0.1.1    Nucleus.localdomain Nucleus" >> /etc/hosts;
arch-chroot /mnt chpasswd <<< "root:gvr74z1l";
arch-chroot /mnt useradd -mU -s /bin/bash -G sys,log,network,floppy,scanner,power,rfkill,users,video,storage,optical,lp,audio,wheel,adm abstract;
arch-chroot /mnt chpasswd <<< "abstract:cipher";
arch-chroot /mnt reflector -c Sweden --age 10 --sort rate --save /etc/pacman.d/mirrorlist;
arch-chroot /mnt;
echo "Edit /etc/mkinitcpio.conf with MODUlES(btrfs)"