mkinitcpio -p linux;
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=Nucleus;
grub-mkconfig -o /boot/grub/grub.cfg;
systemctl enable NetworkManager;
systemctl enable haveged;
systemctl enable gdm;
systemctl enable bluetooth;
systemctl enable org.cups.cupsd;
systemctl enable sshd;
EDITOR=vim visudo
